import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import Container from "@mui/material/Container";
import Navbar from "../Components/Presentation/Navbar";
import BoardsDisplay from "../Components/Container/BoardCard";
import FormDialog from "../Components/Container/CreateBoardDialogBox";
import ToastMessage from "../Components/Presentation/ToastMessage";
import api from "../utils";
import useGetData from "../useGetData";

function HomePage() {
  const [title, setTitle] = useState("");

  const {
    data: boards,
    loding: loader,
    error: error,
    setLoader: setLoader,
    setError: setError,
    fetchApi
  } = useGetData(
    `https://api.trello.com/1/members/me/boards?key=${api.key}&token=${api.token}`,
    "get"
  );

  useEffect(()=>{
    console.log("Hello world------.....HomePage.jsx")
    fetchApi();
  },[])

  const navigate = useNavigate();

  async function handleCreateBoard() {
    try {
      if (title.length > 0) {
        setLoader(true);
        const response = await axios.post(
          `https://api.trello.com/1/boards/?name=${title}&key=${api.key}&token=${api.token}`
        );
        setTitle("");
        navigate(`/board/${response.data.id}`);
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  if (error) {
    return <div className="errorMessage">Unable create a new board!!!</div>;
  }

  return (
    <>
      <Navbar />
      <Container
        maxWidth="false"
        sx={{ background: "#172b4d", height: "93vh" }}
      >
        <div>
          {!loader ? (
            <div>
              <ToastMessage
                message={`Boards data loaded successfully!!!`}
                type={`success`}
              />
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <Typography variant="h3" sx={{ marginTop: 8, color: "white" }}>
                  BOARDS
                </Typography>
                <div
                  style={{
                    marginTop: "75px",
                    border: "1px solid red",
                    marginRight: "30px",
                  }}
                >
                  <FormDialog
                    handleCreateBoard={handleCreateBoard}
                    setTitle={setTitle}
                  />
                </div>
              </div>
              <BoardsDisplay
                boards={boards}
                handleCreateBoard={handleCreateBoard}
                setTitle={setTitle}
                error={error}
              />
            </div>
          ) : (
            <Typography
              variant="h3"
              sx={{
                color: "white",
                display: "flex",
                justifyContent: "center",
                height: "500px",
                alignItems: "center",
              }}
            >
              <CircularProgress color="inherit" />
            </Typography>
          )}
        </div>
      </Container>
    </>
  );
}

export default HomePage;
