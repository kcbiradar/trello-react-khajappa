import axios from "axios";
import { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import Lists from "../Components/Container/Lists";
import Navbar from "../Components/Presentation/Navbar";
import { Button } from "@mui/material";
import CircularProgress from "@mui/material/CircularProgress";
import api from "../utils";
import useGetData from "../useGetData";

function BoardPage() {
  
  const { id } = useParams();

  const {
    data: listsData,
    loading: loader,
    error: error,
    setData: setListsData,
    fetchApi
  } = useGetData(
    `https://api.trello.com/1/boards/${id}/lists?key=${api.key}&token=${api.token}`,
    "get"
  );

  useEffect(()=>{
    console.log("Hello world------.....BoardPage.jsx")
    fetchApi();
  },[])

  if (error) {
    return <div className="errorMessage">Unable to fetch API Data</div>;
  }

  return (
    <>
      <Navbar />
      <div>
        <div>
          {!loader ? (
            <div>
              <NavLink to={"/"}>
                <Button
                  variant="contained"
                  sx={{
                    marginTop: "10px",
                    marginLeft: "10px",
                    width: "100px",
                    background: "#333",
                  }}
                >
                  Back
                </Button>
              </NavLink>
              <Lists
                boardId={id}
                listsData={listsData}
                setListsData={setListsData}
              />
            </div>
          ) : (
            <div style={{ marginTop: "20%", marginLeft: "45%" }}>
              <CircularProgress color="success" />
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default BoardPage;
