import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";

function ProgressBar ({ items }){
  
  const selected_items_length = items.reduce((acc, current) => {
    if (current.state === "complete") acc++;
    return acc;
  }, 0);

  const items_length = items.length;

  const newProgress =
    items.length === 0
      ? 0
      : (selected_items_length / items_length).toFixed(2) * 100;

  return (
    <Box sx={{ width: "100%" }}>
      <span>{newProgress}%</span>
      <LinearProgress
        variant="determinate"
        value={newProgress}
        sx={{ height: "8px" }}
      />
    </Box>
  );
};

export default ProgressBar;
