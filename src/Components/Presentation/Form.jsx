import * as React from "react";
import Button from "@mui/joy/Button";
import Input from "@mui/joy/Input";
import Stack from "@mui/joy/Stack";
import Box from "@mui/material/Box";

export default function Form({ handleCreate, setTitle, placeholder }) {
  return (
    <Box
      my={1}
      display="flex"
      justifyContent="center"
      gap={2}
      p={2}
      sx={{ border: "1px solid grey" }}
    >
      <form
        onSubmit={(event) => {
          event.preventDefault();
          handleCreate();
          event.target.reset();
        }}
      >
        <Stack spacing={1}>
          <div style={{ display: "flex", gap: "5px" }}>
            <Input
              onChange={(event) => {
                setTitle(event.target.value.trim());
              }}
              placeholder={placeholder}
              required
            />
            <Button type="submit"> Add </Button>
          </div>
        </Stack>
      </form>
    </Box>
  );
}
