import React, { useEffect, useState } from "react";
import { styled } from "@mui/joy/styles";
import Sheet from "@mui/joy/Sheet";
import { Card, CardContent, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CircularProgress from "@mui/material/CircularProgress";
import axios from "axios";
import Form from "../Presentation/Form";
import Cards from "./Cards";
import api from "../../utils";
import useGetData from "../../useGetData";

const Item = styled(Sheet)(({ theme }) => ({
  ...theme.typography["body-sm"],
  textAlign: "center",
  fontWeight: theme.fontWeight.md,
  color: theme.vars.palette.text.secondary,
  border: "1px solid",
  borderColor: theme.palette.divider,
  padding: theme.spacing(1),
  borderRadius: theme.radius.md,
  width: "300px",
  flexShrink: "0",
}));

function ListItem({ listData, handleDeleteList }) {
  
  const [cardTitle, setCardTitle] = useState(null);

  const {
    data: allCards,
    loading: loader,
    error: error,
    setData: setAllCards,
    setLoader: setLoader,
    setError: setError,
    fetchApi
  } = useGetData(
    `https://api.trello.com/1/lists/${listData.id}/cards?key=${api.key}&token=${api.token}`,
    "get"
  );

  useEffect(()=>{
    fetchApi()
  },[])

  async function handleCreateCard() {
    try {
      if (cardTitle.length > 0) {
        setLoader(true);
        const response = await axios.post(
          `https://api.trello.com/1/cards?idList=${listData.id}&name=${cardTitle}&key=${api.key}&token=${api.token}`
        );
        setAllCards([...allCards, response.data]);
        setCardTitle("");
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  async function handleDeleteCard(cardId) {
    try {
      setLoader(true);
      await axios.delete(
        `https://api.trello.com/1/cards/${cardId}?key=${api.key}&token=${api.token}`
      );
      setAllCards(allCards.filter((each_card) => each_card.id != cardId));
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  if (error) {
    return <div className="errorMessage">Unable to fetch API Data!!!</div>;
  }

  return (
    <div>
      <Item>
        <Typography
          variant="p"
          sx={{
            display: "flex",
            justifyContent: "space-between",
            fontWeight: "800",
          }}
        >
          {listData.name}
          <DeleteIcon
            sx={{ "&:hover": { color: "red" }, cursor: "pointer" }}
            onClick={() => handleDeleteList(listData.id)}
          />
        </Typography>
        {
          <div>
            <div>
              {allCards.map((card) => (
                <Cards
                  key={card.id}
                  card={card}
                  handleDeleteCard={handleDeleteCard}
                />
              ))}

              <Card sx={{ marginTop: 2 }}>
                <Form
                  handleCreate={handleCreateCard}
                  setTitle={setCardTitle}
                  placeholder={`Enter card name...`}
                />
              </Card>
            </div>
            {loader && <CircularProgress />}
          </div>
        }
      </Item>
    </div>
  );
}

export default ListItem;
