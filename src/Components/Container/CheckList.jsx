import React from "react";
import Checkbox from "@mui/material/Checkbox";
import DialogContent from "@mui/material/DialogContent";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import CheckItems from "./CheckItem";
import Button from "@mui/material/Button";

function CheckList({ checkList, cardId, handleDeleteCheckList }) {

  return (
    <div>
      <DialogContent
        dividers
        sx={{
          background: "gray",
          color: "white",
        }}
      >
        <Box sx={{ display: "flex" }}>
          <Checkbox disabled checked />
          <Typography gutterBottom variant="p" sx={{ marginTop: "12px" }}>
            {checkList.name}
          </Typography>
        </Box>
        <CheckItems checkListId={checkList.id} cardId={cardId} />
        <div>
          <Button
            variant="contained"
            color="error"
            sx={{
              padding: "1px",
              marginTop: "5px",
              marginLeft: "10px",
            }}
            onClick={() => handleDeleteCheckList(checkList.id)}
          >
            Delete
          </Button>
        </div>
      </DialogContent>
    </div>
  );
}

export default CheckList;
