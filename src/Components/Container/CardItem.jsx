import * as React from "react";
import Button from "@mui/material/Button";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Container from "@mui/material/Container";
import CircularProgress from "@mui/material/CircularProgress";
import { useState,useEffect } from "react";
import axios from "axios";
import CheckList from "./CheckList";
import Form from "../Presentation/Form";
import api from "../../utils";
import useGetData from "../../useGetData";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
}));

export default function CardItem({ cardName, cardId }) {
  
  const [open, setOpen] = useState(false);
  const [checkListTitle, setCheckListTitle] = useState(null);

  const {
    data: allCheckLists,
    loading: loader,
    error: error,
    setData: setCheckList,
    setLoader: setLoader,
    setError: setError,
    fetchApi
  } = useGetData(
    `https://api.trello.com/1/cards/${cardId}/checklists?key=${api.key}&token=${api.token}`,
    "get"
  );

  useEffect(()=>{
    fetchApi()
  },[])

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  async function handleCreateCheckList() {
    try {
      if (checkListTitle.length > 0) {
        setLoader(true);
        const response = await axios.post(
          `https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListTitle}&key=${api.key}&token=${api.token}`
        );
        setCheckList([...allCheckLists, response.data]);
        setCheckListTitle("");
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  async function handleDeleteCheckList(checkListId) {
    try {
      setLoader(true);
      await axios.delete(
        `https://api.trello.com/1/checklists/${checkListId}/?key=${api.key}&token=${api.token}`
      );
      setCheckList(
        allCheckLists.filter(
          (each_checkItem) => each_checkItem.id !== checkListId
        )
      );
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  if (error) {
    return <div className="errorMessage">Error occured while fetching API</div>;
  }

  return (
    <Container>
      <Button onClick={handleClickOpen}>{cardName}</Button>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <div>
          {!loader ? (
            <div>
              <DialogTitle
                sx={{ m: 0, p: 2, width: "500px", border: "1px solid black" }}
                id="customized-dialog-title"
              >
                <Form
                  handleCreate={handleCreateCheckList}
                  setTitle={setCheckListTitle}
                  placeholder={`Enter checklist name...`}
                />
              </DialogTitle>
              <IconButton
                aria-label="close"
                onClick={handleClose}
                sx={{
                  position: "absolute",
                  right: 8,
                  top: 20,
                  color: (theme) => theme.palette.grey[500],
                }}
              >
                <CloseIcon />
              </IconButton>
              <Container sx={{ background: "gray" }}>
                {allCheckLists.map((each_checkList) => {
                  return (
                    <CheckList
                      key={each_checkList.id}
                      checkList={each_checkList}
                      cardId={cardId}
                      handleDeleteCheckList={handleDeleteCheckList}
                    />
                  );
                })}
              </Container>
            </div>
          ) : (
            <CircularProgress />
          )}
        </div>
      </BootstrapDialog>
    </Container>
  );
}
