import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Checkbox from "@mui/material/Checkbox";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import axios from "axios";
import { useEffect, useState } from "react";
import ProgressBar from "../Presentation/ProgressBar";
import CircularProgress from "@mui/material/CircularProgress";
import Form from "../Presentation/Form";
import api from "../../utils";
import useGetData from "../../useGetData";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  color: theme.palette.text.secondary,
}));

function CheckItems({ checkListId, cardId }) {
  const [itemTitle, setItemTitle] = useState("");

  const {
    data: checkItems,
    loading: loader,
    error: error,
    setData: setCheckItems,
    setLoader: setLoader,
    setError: setError,
    fetchApi
  } = useGetData(
    `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${api.key}&token=${api.token}`,
    "get"
  );
  useEffect(()=>{
    fetchApi()
  },[])

  async function handleCreateCheckItem() {
    try {
      if (itemTitle.length > 0) {
        setLoader(true);
        const response = await axios.post(
          `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${itemTitle}&key=${api.key}&token=${api.token}`
        );
        setCheckItems([...checkItems, response.data]);
        setItemTitle("");
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  async function toggleCheckItems(checkItemId, status) {
    try {
      setLoader(true);

      await axios.put(
        `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${
          api.key
        }&token=${api.token}&state=${status ? "complete" : "incomplete"}`
      );
      setCheckItems(
        checkItems.map((item) => {
          if (item.id === checkItemId) {
            item.state === "complete"
              ? (item.state = "incomplete")
              : (item.state = "complete");
          }
          return item;
        })
      );
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  async function handleDeleteCheckItem(checkItemId) {
    try {
      setLoader(true);
      await axios.delete(
        `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${api.key}&token=${api.token}`
      );

      setCheckItems(checkItems.filter((item) => item.id !== checkItemId));
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  if (error) {
    return <div className="errorMessage">Error occured while Fetching API</div>;
  }

  return (
    <div>
      <div>
        {!loader ? (
          <div>
            <ProgressBar items={checkItems} />

            {checkItems.map((each_item) => {
              const status = each_item.state === "complete" ? true : false;
              return (
                <Box key={each_item.id} sx={{ flexGrow: 1, marginTop: "5px" }}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Item
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <div>
                          <Checkbox
                            onChange={() =>
                              toggleCheckItems(each_item.id, status)
                            }
                            checked={status}
                          />
                          <span
                            style={{
                              textDecoration: status ? "line-through" : "none",
                            }}
                          >
                            {each_item.name}
                          </span>
                        </div>
                        <div style={{ marginTop: "8px", color: "red" }}>
                          <RemoveCircleOutlineIcon
                            sx={{ cursor: "pointer" }}
                            onClick={() => handleDeleteCheckItem(each_item.id)}
                          />
                        </div>
                      </Item>
                    </Grid>
                  </Grid>
                </Box>
              );
            })}

            <div>
              <Form
                handleCreate={handleCreateCheckItem}
                setTitle={setItemTitle}
                placeholder={`enter item name`}
              />
            </div>
          </div>
        ) : (
          <CircularProgress color="inherit" />
        )}
      </div>
    </div>
  );
}

export default CheckItems;
