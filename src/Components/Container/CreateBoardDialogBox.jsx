import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Form from "../Presentation/Form";

export default function FormDialog({ handleCreateBoard, setTitle }) {
  
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Button onClick={handleClickOpen} style={{ color: "white" }}>
        Create a new board
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogContent sx={{ height: "150px", width: "500px" }}>
          <DialogContentText>Board</DialogContentText>
          <Form
            handleCreate={handleCreateBoard}
            setTitle={setTitle}
            placeholder={"Enter board name..."}
          />
          <Button onClick={handleClose} >Close</Button>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
