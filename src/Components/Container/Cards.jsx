import React from "react";
import { Card, CardContent } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import CardItem from "./CardItem";
function Cards({ card,handleDeleteCard }) {
  return (
    <div>
      <div key={card.id}>
        <Card key={card.id} sx={{ marginTop: 2 }}>
          <CardContent
            sx={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <CardItem cardName={card.name} cardId={card.id} />
            <DeleteIcon
              sx={{
                cursor: "pointer",
                marginTop: "7px",
                "&:hover": { color: "red" },
              }}
              onClick={() => handleDeleteCard(card.id)}
            />
          </CardContent>
        </Card>
      </div>
    </div>
  );
}

export default Cards;
