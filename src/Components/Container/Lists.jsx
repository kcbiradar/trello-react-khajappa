import * as React from "react";
import Divider from "@mui/joy/Divider";
import Stack from "@mui/joy/Stack";
import Box from "@mui/joy/Box";
import CircularProgress from "@mui/material/CircularProgress";
import { Typography } from "@mui/material";
import axios from "axios";
import { useState , useEffect} from "react";
import Form from "../Presentation/Form";
import ListItem from "./ListItem";
import api from "../../utils";

export default function Lists({ boardId, listsData, setListsData }) {
  
  const [loader, setLoader] = useState(false);
  const [error, setError] = useState(null);
  const [listTitle, setListTitle] = useState("");

  async function handleCreateList() {
    try {
      if (listTitle.length > 0) {
        setLoader(true);
        const response = await axios.post(
          `https://api.trello.com/1/lists?name=${listTitle}&idBoard=${boardId}&key=${api.key}&token=${api.token}`
        );
        setListsData([...listsData, response.data]);
        setListTitle("");
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  async function handleDeleteList(listId) {
    setLoader(true);
    try {
      await axios.put(
        `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${api.key}&token=${api.token}`
      );
      setListsData(listsData.filter((each_list) => each_list.id !== listId));
    } catch (error) {
      setError(error.message);
    } finally {
      setLoader(false);
    }
  }

  if (error) {
    return <div className="errorMessage">Unable to fetch API </div>;
  }

  return (
    <Box sx={{ width: "100%", background: "#172b4d", height: "100vh" }}>
      <Form
        handleCreate={handleCreateList}
        setTitle={setListTitle}
        placeholder={`Enter list name...`}
      />
      <Typography variant="h4" color="white" margin={"5px"}>
        Lists
      </Typography>
      <Stack>
        <div style={{ position: "relative" }}>
          {loader && (
            <CircularProgress
              sx={{ position: "absolute", top: "50%", left: "50%" }}
            />
          )}

          <Stack
            direction="row"
            spacing={2}
            justifyContent="flex-start"
            alignItems="flex-start"
            overflow="auto"
            divider={<Divider orientation="vertical" />}
            height={"70vh"}
          >
            {listsData.map((each_list) => {
              return (
                <ListItem
                  key={each_list.id}
                  listData={each_list}
                  handleDeleteList={handleDeleteList}
                />
              );
            })}
          </Stack>
        </div>
      </Stack>
    </Box>
  );
}
