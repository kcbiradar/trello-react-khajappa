import * as React from "react";
import { styled } from "@mui/joy/styles";
import Sheet from "@mui/joy/Sheet";
import Grid from "@mui/joy/Grid";
import Typography from "@mui/material/Typography";
import { NavLink } from "react-router-dom";
import ToastMessage from "../Presentation/ToastMessage";

const Item = styled(Sheet)(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "white"
      ? theme.palette.background.level1
      : "#328FDF",
  ...theme.typography["body-sm"],
  padding: theme.spacing(1),
  textAlign: "center",
  borderRadius: 4,
  height: 100,
  width: 220,
  color: "White",
  fontFamily: "sans-serif",
  fontSize: "30",
  display: "flex",
  alignItems: "center",
}));

export default function BoardCard({ boards }) {
  return (
    <Grid container spacing={10} sx={{ flexGrow: 1, marginTop: 1 }}>
      {boards.length === 0 ? (
        <Typography
          variant="h3"
          sx={{
            textAlign: "center",
            color: "white",
            marginLeft: "42px",
            marginTop: "100px",
          }}
        >
          No boards, please add the boards
        </Typography>
      ) : (
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          {boards.map((each_board) => {
            return (
              <NavLink to={`board/${each_board.id}`} key={each_board.id}>
                <Grid xs={4}>
                  <Item>{each_board.name}</Item>
                </Grid>
              </NavLink>
            );
          })}
          {boards.length >= 10 ? (
            <ToastMessage
              message={`Board creating limit is exceeded`}
              type={`error`}
            />
          ) : (
            ""
          )}
        </div>
      )}
    </Grid>
  );
}
