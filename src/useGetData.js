import { useState } from "react";

import axios from "axios";

const useGetData = (urlPath, method = "get") => {
  const [data, setData] = useState([]);
  const [loading, setLoader] = useState(false);
  const [error, setError] = useState(false);

  const fetchApi = async () => {
    try {
      setLoader(true);
      const response = await axios({
        method,
        url: urlPath,
      });
      setData(response.data);
    } catch (error) {
      setError(true);
    } finally {
      setLoader(false);
    }
  };

  return { data, loading, error, setData, setLoader, setError, fetchApi };
};

export default useGetData;
